import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadStdentsComponent } from './load-stdents.component';

describe('LoadStdentsComponent', () => {
  let component: LoadStdentsComponent;
  let fixture: ComponentFixture<LoadStdentsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoadStdentsComponent]
    });
    fixture = TestBed.createComponent(LoadStdentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
