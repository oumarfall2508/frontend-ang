import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  public loginForm! : FormGroup;

constructor(private formbuilder: FormBuilder, private authService: AuthService, private router: Router){

}

  ngOnInit(): void {
    this.loginForm=this.formbuilder.group(
      {
        username: this.formbuilder.control(''),
        password: this.formbuilder.control('')
      }
    );
  }

  login(){
    let username = this.loginForm.value.username;
    let password = this.loginForm.value.password;
    if(this.authService.login(username, password)==true){
      this.router.navigateByUrl("/admintemplate");
    }
    else{
     // this.router.navigateByUrl("/admintemplate");
    }

  }

}
