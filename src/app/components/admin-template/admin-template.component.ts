import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-admin-template',
  templateUrl: './admin-template.component.html',
  styleUrls: ['./admin-template.component.css']
})
export class AdminTemplateComponent implements OnInit{

  constructor(private router: Router, public auth: AuthService){}

  ngOnInit(): void {
    //throw new Error('Method not implemented.');
  }


  logout(){
      this.auth.logout();
      this.router.navigateByUrl("/login")
  }

}
