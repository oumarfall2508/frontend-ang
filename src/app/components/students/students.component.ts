import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortHeader } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { StudentsService } from 'src/app/services/students.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit, AfterViewInit{
  public students : any;
  public displayedColumns: string[] = ["id", "firstName", "programId", "payments"];
  public dataSource : any;
  @ViewChild(MatPaginator) paginator! : MatPaginator;
  @ViewChild(MatSort) sort! : MatSort;

  constructor(private router: Router, private studentsService: StudentsService){

  }
  ngOnInit(): void {
    this.students= [];
    this.studentsService.getStudents().subscribe({
      next: data=>{
         this.students=data;
         this.dataSource = new MatTableDataSource(this.students);

         this.dataSource.paginator=this.paginator;
         this.dataSource.sort=this.sort;
      },
      error: error=>{
          console.log(error);
      }
    });

   /*  this.students= [] ;
    for(let i: number = 1; i <100; i++){
      this.students.push(
        {id: i, firstName: Math.random().toString(20), lastName: Math.random().toString(20)}
        );
    } 
    this.dataSource = new MatTableDataSource(this.students);
    */
    
  }

  ngAfterViewInit(): void {
    //this.dataSource.paginator=this.paginator;
    //this.dataSource.sort=this.sort;
  }

  filtrerData(event: Event){
    let value: string = (event.target as HTMLInputElement).value;
    //alert(value);
    this.dataSource.filter = value;

  }

  getPayments(student: any){
      this.router.navigateByUrl("admintemplate/payments")
  }


}
