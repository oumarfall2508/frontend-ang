import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private http: HttpClient) { }

  /**
   * getStudents
 : Observable  */
  public getStudents(): Observable<any[]> {
    return this.http.get<any[]>("http://localhost:8082/students");
  }
}
