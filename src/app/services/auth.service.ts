import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public users : any = {
    admin: {password:"1234", roles: ['ADMIN']},
    user1: {password:"1234", roles: ['STUDENT', 'USER']},
    user2: {password:"1234", roles: ['USER']}
  }

  public isAuthenticated: boolean = false;
  public username: any;
  public roles: string[]=[];

  constructor() { }

  public login(username: string, password: string): boolean{
    if(this.users[username] && this.users[username]['password']==password){
      this.username = username;
      this.isAuthenticated = true ;
      this.roles = this.users[username]['roles'];

      return true;
    }

    else{
      return false;
    }
  
  }

  logout(): void{
      this.isAuthenticated=false;
      this.username=undefined;
      this.roles=[]
  }

}
