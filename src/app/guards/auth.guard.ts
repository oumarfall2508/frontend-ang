import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Inject, inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state) => {
  let auth: AuthService = inject(AuthService);
  let router: Router = inject(Router);
  if(auth.isAuthenticated){
    return true;
  }
  else{
    router.navigateByUrl("");
    return false;
  }
};
