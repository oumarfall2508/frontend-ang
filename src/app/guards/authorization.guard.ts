import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { inject } from '@angular/core';
import { of } from 'rxjs';

export const authorizationGuard: CanActivateFn = (route, state) => {
  let auth: AuthService = inject(AuthService);
  let router: Router = inject(Router);
  if(auth.isAuthenticated){
    let authenticatUserRoles  = auth.roles;
    let requiredRoles = route.data["roles"];
   for(let role of authenticatUserRoles){
      if(requiredRoles.includes(role)){
         return true;
      }
    }
    return false;
  }
  else{
    router.navigateByUrl("");
    return false;
  }
};
