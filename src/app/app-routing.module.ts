import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { StudentsComponent } from './components/students/students.component';
import { LoginComponent } from './components/login/login.component';
import { PaymentsComponent } from './components/payments/payments.component';
import { DashBoardComponent } from './components/dash-board/dash-board.component';
import { LoadPaymentsComponent } from './components/load-payments/load-payments.component';
import { LoadStdentsComponent } from './components/load-stdents/load-stdents.component';
import { AdminTemplateComponent } from './components/admin-template/admin-template.component';
import { authGuard } from './guards/auth.guard';
import { authorizationGuard } from './guards/authorization.guard';

const routes: Routes = [
  {path : "admintemplate", component : AdminTemplateComponent,
   canActivate:[authGuard],
   children : [
    {path : "home", component : HomeComponent},
    {path : "profile", component : ProfileComponent},
    {path : "students", component : StudentsComponent},
    {path : "payments", component : PaymentsComponent},
    {path : "dashBoard", component : DashBoardComponent},
    {path : "loadpayments", component : LoadPaymentsComponent,
    canActivate:[authorizationGuard],
     data : {roles:"ADMIN"}
    },
    {path : "loadstudents", component : LoadStdentsComponent,
    canActivate:[authorizationGuard],
    data : {roles:"ADMIN"}
    }
  ]},
  
  {path : "login", component : LoginComponent},
  {path : "", component : LoginComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
